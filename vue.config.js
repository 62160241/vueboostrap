module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160241/learn_bootstrap/'
    : '/'
}
